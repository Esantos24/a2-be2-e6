**M3**- Programació                                         

Eric Santos Cortada                                                                                             

Professor/a: Toni Pifarré Mata

**Exercici 1:**

**Justifica la utilització de les estructures iteratives que coneixes i posa’n exemples.** 

Les estructures iteratives serveixen per a que mentres una expressio boleana és compleixi repeteixi unes instruccions com per exemple avançar posicions,..

```
Ex: 
while(text[i]==' '){
    i++;
}
```
___

**Exercici 2:**

**Disseny d’un algorisme que ens demani l’edat 100 vegades.**


![](Exercicis/ex2.1.png)&emsp;&emsp;![](Exercicis/ex2.2.png)
                                                                
___

**Exercici 3:**

**Disseny d’un algorisme que mostri els 100 primers números naturals.**

![](Exercicis/ex3.png)

___

**Exercici 4:** 

**Disseny d’un algorisme que mostri els nombres parells de l’1 al 100.** 

![](Exercicis/ex4.png)

___

**Exercici 5:** 

**Disseny d’un algorisme que mostri els nombres senars de l’1 al 100.**

![](Exercicis/ex5.png)

___

**Exercici 6:** 

**Disseny d’un algorisme que mostri els nombres primers que hi ha entre l’1 i el 100 inclosos.** 

![](Exercicis/ex6.png)

___

**Exercici 7:** 

**Disseny d’un algorisme que compti de 5 en 5 fins a 100.** 

![](Exercicis/ex7.png)

___

**Exercici 8:** 

**Disseny d’un algorisme que vagi del número 100 al número 1 de forma descendent.** 

![](Exercicis/ex8.png)

___

**Exercici 9:** 

**Disseny d’un algorisme que mostri “n” termes de la successió de Fibonacci. Els dos primers termes valen 1 i el terme n-èssim és la suma dels dos anteriors. És a dir, an = an- 1 + an-2.** 

![](Exercicis/ex9.1.png)&emsp;&emsp;![](Exercicis/ex9.2.png)

___

**Exercici 10:** 

**A partir de la següent especificació: { anys=ANYS ^ dies=DIES ^ ^hores=HORES ^ minuts= MINUTS ^ segons=SEGONS ^ anys>0 ^ ^0<=dies<365 ^ 0<=hores<24 ^0<=minuts<60 ^ 0<=segons<60 }. Dissenyeu un algorisme que incrementi el temps en un segon.** 

![](Exercicis/ex10.1.png)&emsp;&emsp;![](Exercicis/ex10.2.png)

___

**Exercici 11:** 

**Disseny d’un algorisme que calculi les solucions d’una equació de 2n. grau.** 

![](Exercicis/ex11.1.png)&emsp;&emsp;![](Exercicis/ex11.2.png)

___

**Exercici 12:** 

**Donat un text calcular la seva longitud. Recorda que un text, en C, acaba amb el caràcter de final de cadena ‘\0’. A partir d’ara “text” i/o “paraula” ho llegirem com cadena de caràcters o string.** 

![](Exercicis/ex12.png)

___

**Exercici 13:** 

**Donat un text comptar el nombre de vocals.** 

![](Exercicis/ex13.1.png)&emsp;&emsp;![](Exercicis/ex13.2.png)

___

**Exercici 14:**

**Donat un text comptar el nombre de consonants.** 

![](Exercicis/ex14.1.png)&emsp;&emsp;![](Exercicis/ex14.2.png)

___

**Exercici 15:** 

**Donat un text capgirar-lo.** 

![](Exercicis/ex15.png)

___

**Exercici 16:** 

**Donat un text comptar el nombre de paraules que acaben en “ts”.** 

![](Exercicis/ex16.1.png)&emsp;&emsp;![](Exercicis/ex16.2.png)

___

**Exercici 17:** 

**Donat un text comptar el nombre de paraules.** 

![](Exercicis/ex17.1.png)&emsp;&emsp;![](Exercicis/ex17.2.png)

___

**Exercici 18:** 

**Donat un text dissenyeu un algorisme que compti els cops que apareixen conjuntament la parella de caràcters “as” dins del text.** 
                                                                    
![](Exercicis/ex18.1.png)&emsp;&emsp;![](Exercicis/ex18.2.png)

___

**Exercici 19:** 

**Donat un text i una paraula (anomenada parbus). Dissenyeu un algorisme que comprovi si la paraula és troba dins del text.** 

![](Exercicis/ex19.1.png)&emsp;![](Exercicis/ex19.2.png)&emsp;![](Exercicis/ex19.3.png)

**Nota: parbus és el nom de la variable que conté la paraula a cercar.** 

___

**Exercici 20:** 

**Donat un text i una paraula (parbus). Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text.**

![](Exercicis/ex20.1.png)&emsp;![](Exercicis/ex20.2.png)&emsp;![](Exercicis/ex20.3.png)

___

**Exercici 21:** 

**Donat un text i una paraula (parbus) i una paraula (parsub). Dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (parbus) per la paraula (parsub).** 

![](Exercicis/ex21.1.png)&emsp;&emsp;![](Exercicis/ex21.2.png)&emsp;&emsp;![](Exercicis/ex21.3.png)

**Nota: parbus és el nom de la variable que conté la paraula a buscar i parsub el nom de la variable que conté la paraula que volem substituir.** 

